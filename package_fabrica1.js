package fabrica1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class fabrica1 {

	@BeforeClass
	public static void testandofabrica1() {
	}
	
	@AfterClass
	public static void finalizandofabrica1() {
	}
	
	@Test
	public void fluxofabrica1() {
		
		WebDriverManager.chromiumdriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
	
		driver.findElement(By.name("username"));
		driver.findElement(By.name("passwod"));
		driver.findElement(By.name("comments"));
		
		WebElement checkbox = driver.findElement(By.xpath("//input[@value='cb']"));
		
		WebElement dropdown = driver.findElement(By.name("dropdown"));
		new Select(dropdown);
				
	    WebElement radio = driver.findElement(By.xpath("//input[@value='rd']"));
	    
	    WebElement buttonSubmit = driver.findElement(By.xpath("//input[@value='submit']"));
	    
	    checkbox.click();
	    
	    radio.click();
	    
	    buttonSubmit.click();
	
	}
}
