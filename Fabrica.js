package fabrica;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ISelect;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;
import junit.framework.Assert;

public class fabrica {
	
	@BeforeClass
	public static void preparandofabrica() {	
	}
	
     @AfterClass
     public static void finalizandofabrica() {   	 
     }
     
     @SuppressWarnings("deprecation")
	@Test
     public void fabricaFeli() {
    	 WebDriverManager .chromiumdriver().setup();
    	 ChromeDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    	 
		WebElement userNameBox = driver.findElement(By.name("username"));
		WebElement passwordBox = driver.findElement(By.name("password"));
		WebElement commentsBox = driver.findElement(By.name("comments"));
		
		WebElement checkbox1 = driver.findElement(By.xpath("//input[@value='cb1']"));
		WebElement checkbox2 = driver.findElement(By.xpath("//input[@value='cb2']"));
		WebElement checkbox3 = driver.findElement(By.xpath("//input[@value='cb3']"));
		
		WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
		Select selectMultipleSelect = new  Select (multipleSelect);
		
		WebElement dropdown = driver.findElement(By.name("dropdown"));
		Select selectDropdown = new Select(dropdown);
		
		WebElement radio2 = driver.findElement(By.xpath("//input[@value='submit']"));
		
		WebElement buttonSubmit = driver.findElement(By.xpath("//input[@value='submit']"));
		
		userNameBox.sendKeys("jonathan");
		passwordBox.sendKeys("arlley");
		commentsBox.sendKeys("Qa/teste");
		checkbox1.click();
		checkbox2.click();
		checkbox3.click();
		selectDropdown.selectByValue("dd4");
		radio2.click();
		
		buttonSubmit.click();
		
		Assert.assertEquals("jonathan", driver.findElement(By.id("_valueusername")).getText());
		Assert.assertEquals("jonathan", driver.findElement(By.id("_valuepassword")).getText());

		
     }
}
